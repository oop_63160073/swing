/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadoninter.swing1;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 *
 * @author Acer
 */
public class HelloMe {
    
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(640, 360);
        
        JLabel lblYName = new JLabel("Your Name:", JLabel.CENTER);
        lblYName.setSize(80, 30);
        lblYName.setLocation(30, 30);
        lblYName.setBackground(Color.magenta);
        lblYName.setOpaque(true);
        
        JTextField txtName = new JTextField();
        txtName.setSize(200, 30);
        txtName.setLocation(120, 30);
        
        JButton btnHello = new JButton("Hello");
        btnHello.setSize(200, 60);
        btnHello.setLocation(220, 150);
        
        JLabel lblHello = new JLabel("Hello...", JLabel.CENTER);
        lblHello.setSize(290, 60);
        lblHello.setLocation(175, 250);
        
        frame.setLayout(null);
        
        frame.add(lblYName);
        frame.add(txtName);
        frame.add(btnHello);
        frame.add(lblHello);
        
        btnHello.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                lblHello.setText("Hello " + txtName.getText() + ".");
            }
        });
        
        frame.setVisible(true);
    }
}
