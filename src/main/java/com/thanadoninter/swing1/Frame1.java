/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadoninter.swing1;

import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author Acer
 */
public class Frame1 {

    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(640, 360);
        
        JLabel lblHello = new JLabel("Hello World!!",JLabel.CENTER);
        lblHello.setBackground(Color.cyan);
        lblHello.setOpaque(true);
        
        frame.add(lblHello);
        frame.setVisible(true);
    }

}
